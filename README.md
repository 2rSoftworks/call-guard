# About

Call Guard is an Android application developed during an Android Programming project at the University of Freiburg. With your permission, Meeting Guard scans through your active calendars for events with their flag set to "busy". During these events all incoming phone calls and text messages will be muted. Optionally you can let your caller know with an outgoing text-message that you are busy and that you will respond as soon as you can. Missed phone calls and text messages will be logged within the application.

# Status

As I no longer use an Android phone I no longer maintain this application. Feel free to fork it or contribute to it if you are interested.