package sabel.meeting_guard.db;

import java.util.ArrayList;
import java.util.logging.Logger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import sabel.meeting_guard.datatypes.Call;
import sabel.meeting_guard.util.Intents;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class DatabaseHandler extends BroadcastReceiver {

  public static enum DB_REQUEST_TYPE {
	ADD_CALL, REMOVE_CALL, REMOVE_ALL, GET_ALL
  }

  public static enum DB_RESPONSE_TYPE {
	CALL_LOG
  }

  protected Logger log = Logger.getLogger(getClass().getSimpleName());

  private TableCalls calls;

  public DatabaseHandler(Context context) {
	calls = new TableCalls(context);

  }

  @Override
  public void onReceive(Context context, Intent intent) {
	if (!isValidIntent(intent)) {
	  log.warning("Received invalid intent in " + getClass().getSimpleName());
	  return;
	}

	Bundle extras = intent.getExtras();
	DB_REQUEST_TYPE type = (DB_REQUEST_TYPE) extras
		.getSerializable(Intents.KEY_REQUEST_TYPE);

	switch (type) {
	case ADD_CALL:
	  if (extras.containsKey(Intents.KEY_REQUEST_CONTENT)) {
		Call call = (Call) extras.get(Intents.KEY_REQUEST_CONTENT);
		calls.addCall(call);
	  }
	  else
		log.warning("Adding call failed because call information was missing");
	  break;
	case REMOVE_CALL:
	  if (extras.containsKey(Intents.KEY_REQUEST_CONTENT)) {
		Call call = (Call) extras.get(Intents.KEY_REQUEST_CONTENT);
		calls.remove(call);
	  }
	  else
		log.warning("Removing call failed because call information was missing");
	  break;
	case REMOVE_ALL:
	  calls.removeAll();
	  break;
	case GET_ALL:
	  Intent callLogIntent = new Intent(Intents.ACTION_DATABASE_RESPONSE);
	  ArrayList<Call> allCalls = calls.getAllCalls();
	  callLogIntent.putExtra(Intents.KEY_RESPONSE_TYPE,
		  DB_RESPONSE_TYPE.CALL_LOG);
	  callLogIntent.putExtra(Intents.KEY_RESPONSE_CONTENT, allCalls);
	  context.sendBroadcast(callLogIntent);
	default:
	  break;
	}

  }

  /**
   * Validate that the intents extras and action are correct.
   * 
   * @param intent
   * @return
   */
  private boolean isValidIntent(Intent intent) {
	if (intent.getExtras() == null)
	  return false;
	if (!intent.getExtras().containsKey(Intents.KEY_REQUEST_TYPE))
	  return false;
	if (!(intent.getExtras().get(Intents.KEY_REQUEST_TYPE) instanceof DB_REQUEST_TYPE))
	  return false;
	if (!intent.getAction().equals(Intents.ACTION_DATABASE_REQUEST))
	  return false;

	// If all checks are positive the intent is valid for this class
	return true;
  }

}
