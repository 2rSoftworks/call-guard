package sabel.meeting_guard.db;

import java.util.ArrayList;
import java.util.logging.Logger;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;
import sabel.meeting_guard.datatypes.Call;
import sabel.meeting_guard.util.DateUtil;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class TableCalls extends SQLiteOpenHelper {

  // Database Name
  protected static String DATABASE_NAME = "meeting_guard";

  // Reminders table name
  protected static String TABLE_CALL_LOG = "call_log";

  // Database Version
  private static int DATABASE_VERSION = 2;

  protected Logger log = Logger.getLogger(getClass().getSimpleName());

  // These columns simplify changes to the database a lot. Simply add a new
  // column and update ALL_COLUMNS_STRING, ALL_COLUMNS, buildValues() in this
  // class and Reminder.reminderFromCursor() in {@link Reminders}, increment the
  // DATABASE_VERSION and you are done.
  public final static Column ID = new Column("_id", 0,
	  "INTEGER PRIMARY KEY AUTOINCREMENT");
  public final static Column NUMBER = new Column("number", 1, "TEXT");
  public final static Column TIME = new Column("time", 2, "STRING");
  public final static Column ACTION = new Column("action", 3, "String");

  protected final static String[] ALL_COLUMNS_STRING = { ID.getKey(),
	  NUMBER.getKey(), TIME.getKey(), ACTION.getKey() };
  protected final static Column[] ALL_COLUMNS = { ID, NUMBER, TIME, ACTION };

  /**
   * 
   * @param context
   * @param name
   * @param factory
   * @param version
   */
  public TableCalls(Context context, String name, CursorFactory factory,
	  int version) {
	super(context, name, factory, version);
  }

  /**
   * 
   * @param context
   */
  public TableCalls(Context context) {
	super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  /**
   * {@inheritDoc}
   * 
   * Create new table with keys defined in this class. Not safe if table exists.
   */
  @Override
  public void onCreate(SQLiteDatabase db) {
	StringBuilder s = new StringBuilder();
	s.append("CREATE TABLE " + TABLE_CALL_LOG + "(");

	for (int i = 0; i < ALL_COLUMNS.length; i++) {
	  s.append(ALL_COLUMNS[i].getKey() + " " + ALL_COLUMNS[i].getType());

	  // insert a comma for all except the last entry
	  if (i < ALL_COLUMNS.length - 1)
		s.append(" , ");
	}

	s.append(")");
	db.execSQL(s.toString());
  }

  /**
   * {@inheritDoc}
   * 
   * Drop table if exists and recreate db.
   */
  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	// Drop older table if existed
	Log.i("SQLITE", "Dropping old table");
	db.execSQL("DROP TABLE IF EXISTS " + TABLE_CALL_LOG);

	// Create tables again
	onCreate(db);
  }

  /**
   * Add a new call to the database.
   * 
   * @param call
   */
  protected void addCall(Call call) {

	log.info("Inserting: " + call.toString());

	// Open a writable database connection
	SQLiteDatabase db = this.getWritableDatabase();

	// Create a container class for the values to be inserted
	ContentValues values = new ContentValues();

	// Insert all values
	values.put(NUMBER.getKey(), call.getNumber());
	values.put(TIME.getKey(), DateUtil.timeAndDateToString(call.getDate()));
	values.put(ACTION.getKey(), call.getAction());

	db.insert(TABLE_CALL_LOG, null, values);

	log.info("Added call to db");
  }

  /**
   * Remove a single call from the database.
   * 
   * @param call
   */
  protected void remove(Call call) {
	SQLiteDatabase db = this.getWritableDatabase();

	// Remove all elements that are marked as completed
	db.delete(TABLE_CALL_LOG, ID.getKey() + " = " + call.getId(), null);

	// Close the connection
	db.close();
  }

  /**
   * Remove all calls from the database.
   */
  protected void removeAll() {
	SQLiteDatabase db = this.getWritableDatabase();

	// Remove all elements that are marked as completed
	db.delete(TABLE_CALL_LOG, null, null);

	// Close the connection
	db.close();
  }

  protected ArrayList<Call> getAllCalls() {
	ArrayList<Call> result = new ArrayList<Call>();
	SQLiteDatabase db = this.getReadableDatabase();
	// Get the reminder id's in ascending order
	Cursor cursor = db.query(TABLE_CALL_LOG, ALL_COLUMNS_STRING, null, null,
		null, null, ID.getKey() + " ASC");

	// Add all id's in the table to the list
	while (cursor.moveToNext()) {
	  result.add(Call.CallFromCursor(cursor));
	}

	for (Call call : result) {
	  log.info("Call: " + call.toString());
	}

	// Close db connection and return the list
	db.close();
	return result;
  }

}
