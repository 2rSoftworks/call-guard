package sabel.meeting_guard.db;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.logging.Logger;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.util.Pair;
import sabel.meeting_guard.datatypes.Calendar;
import sabel.meeting_guard.datatypes.Event;
import sabel.meeting_guard.util.Intents;
import sabel.meeting_guard.util.PreferencesResolver;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This class provides an intent interface to the build in calendars. You can request data by
 * sending an intent containing the correct CAL_REQUEST_TYPE as an extra.
 * 
 * @author Leander
 * 
 */
public class CalendarInterface extends BroadcastReceiver {

  protected Logger log = Logger.getLogger(getClass().getSimpleName());

  // Enumerate types for send and received intents
  public static enum CAL_REQUEST_TYPE {
	UPDATE, GET_CALENDARS, GET_EVENTS
  }

  public static enum CAL_RESPONSE_TYPE {
	RETURN_CALENDARS, RETURN_EVENTS
  }

  // Access fields for the calendar API
  private static final Pair<String, Integer> ACCOUNT_ID = new Pair<String, Integer>(Calendars._ID,
	  0);
  private static final Pair<String, Integer> ACCOUNT_NAME = new Pair<String, Integer>(
	  Calendars.ACCOUNT_NAME, 1);
  private static final Pair<String, Integer> ACCOUNT_DISPLAY_NAME = new Pair<String, Integer>(
	  Calendars.CALENDAR_DISPLAY_NAME, 2);
  private static final Pair<String, Integer> ACCOUNT_OWNER_ACCOUNT = new Pair<String, Integer>(
	  Calendars.OWNER_ACCOUNT, 3);

  private static final Pair<String, Integer> EVENT_EVENT_ID = new Pair<String, Integer>(Events._ID,
	  0);
  private static final Pair<String, Integer> EVENT_CALENDAR_ID = new Pair<String, Integer>(
	  Events.CALENDAR_ID, 1);
  private static final Pair<String, Integer> EVENT_AVAILABILITY = new Pair<String, Integer>(
	  Events.AVAILABILITY, 2);
  private static final Pair<String, Integer> EVENT_START_TIME = new Pair<String, Integer>(
	  Events.DTSTART, 3);
  private static final Pair<String, Integer> EVENT_END_TIME = new Pair<String, Integer>(
	  Events.DTEND, 4);
  private static final Pair<String, Integer> EVENT_TITLE = new Pair<String, Integer>(Events.TITLE,
	  5);

  private static final String[] CALENDAR_INFORMATION = new String[] { ACCOUNT_ID.first,
	  ACCOUNT_NAME.first, ACCOUNT_DISPLAY_NAME.first, ACCOUNT_OWNER_ACCOUNT.first };
  private static final String[] EVENT_INFORMATION = { EVENT_EVENT_ID.first,
	  EVENT_CALENDAR_ID.first, EVENT_AVAILABILITY.first, EVENT_START_TIME.first,
	  EVENT_END_TIME.first, EVENT_TITLE.first };

  private Context context;
  private ArrayList<Calendar> calendars;

  public CalendarInterface(Context context) {
	this.context = context;
	updateAvailableCalendars();
  }

  /**
   * Ask the calendar API for the newest version of the available calendars.
   */
  private void updateAvailableCalendars() {
	Cursor cursor = null;
	ContentResolver contetResolver = context.getContentResolver();
	Uri uri = Calendars.CONTENT_URI;

	// Submit the query and get a Cursor object back.
	cursor = contetResolver.query(uri, CALENDAR_INFORMATION, null, null, null);

	calendars = new ArrayList<Calendar>();

	while (cursor.moveToNext()) {
	  // Get the field values
	  long calID = cursor.getLong(ACCOUNT_ID.second);
	  String displayName = cursor.getString(ACCOUNT_DISPLAY_NAME.second);
	  String accountName = cursor.getString(ACCOUNT_NAME.second);
	  String ownerName = cursor.getString(ACCOUNT_OWNER_ACCOUNT.second);

	  Calendar cal = new Calendar(calID, accountName, displayName, ownerName);
	  calendars.add(cal);
	}

	cursor.close();
  }

  /**
   * Load the available events from the calendar that fall within the range of +- queryIntervall.
   * 
   * @param queryIntervall
   *          time interval to check in milliseconds.
   */
  private PriorityQueue<Event> readEvents(long queryIntervall) {
	Cursor cursor = null;
	PriorityQueue<Event> events = new PriorityQueue<Event>();
	ContentResolver contetResolver = context.getContentResolver();
	Uri uri = Events.CONTENT_URI;

	// Look for the calendars to be watched
	Set<Long> filter = PreferencesResolver.getWatchedCalendars(context);

	String selection = EVENT_START_TIME.first + " > ? " + " AND " + EVENT_START_TIME.first + " < ?";
	String[] selectionArgs = { Long.toString(System.currentTimeMillis() - queryIntervall),
		Long.toString(System.currentTimeMillis() + queryIntervall) };

	cursor = contetResolver.query(uri, EVENT_INFORMATION, selection, selectionArgs, null);

	while (cursor.moveToNext()) {
	  long eventId = cursor.getLong(EVENT_EVENT_ID.second);
	  long calId = cursor.getLong(EVENT_CALENDAR_ID.second);
	  int availability = cursor.getInt(EVENT_AVAILABILITY.second);
	  long startTime = cursor.getLong(EVENT_START_TIME.second);
	  long endTime = cursor.getLong(EVENT_END_TIME.second);
	  String title = cursor.getString(EVENT_TITLE.second);
	  Event event = new Event(eventId, calId, availability, startTime, endTime, title);

	  if (filter.contains(calId) && availability == Events.AVAILABILITY_BUSY) {
		events.add(event);
	  }

	}
	cursor.close();
	return events;

  }

  /**
   * Get the list of all available calendars.
   * 
   * @return
   */
  private ArrayList<Calendar> getAvailableCalendars() {
	return calendars;
  }

  /**
   * This receiver can update or return the list of available calendars.
   */
  @Override
  public void onReceive(Context context, Intent intent) {

	if (isValidIntent(intent)) {

	  Bundle extras = intent.getExtras();
	  CAL_REQUEST_TYPE type = (CAL_REQUEST_TYPE) extras.get(Intents.KEY_REQUEST_TYPE);

	  if (type == CAL_REQUEST_TYPE.UPDATE) {
		updateAvailableCalendars();
	  }
	  else if (type == CAL_REQUEST_TYPE.GET_CALENDARS) {
		Intent calendarIntent = new Intent(Intents.ACTION_CALENDAR_RESPONSE);
		calendarIntent.putExtra(Intents.KEY_RESPONSE_TYPE, CAL_RESPONSE_TYPE.RETURN_CALENDARS);
		calendarIntent.putExtra(Intents.KEY_RESPONSE_CONTENT, getAvailableCalendars());
		context.sendBroadcast(calendarIntent);
	  }
	  else if (type == CAL_REQUEST_TYPE.GET_EVENTS) {
		Long duration = (Long) extras.get(Intents.KEY_REQUEST_CONTENT);
		PriorityQueue<Event> events = readEvents(duration);

		Intent eventIntent = new Intent(Intents.ACTION_CALENDAR_RESPONSE);
		eventIntent.putExtra(Intents.KEY_RESPONSE_TYPE, CAL_RESPONSE_TYPE.RETURN_EVENTS);
		eventIntent.putExtra(Intents.KEY_RESPONSE_CONTENT, events);
		context.sendBroadcast(eventIntent);
	  }

	}
	else {
	  log.warning("Received invalid intent in Calendar Interface");
	}

  }

  /**
   * Validate that the intents extras and action are correct.
   * 
   * @param intent
   * @return
   */
  private boolean isValidIntent(Intent intent) {
	if (intent.getExtras() == null)
	  return false;
	if (!intent.getExtras().containsKey(Intents.KEY_REQUEST_TYPE))
	  return false;
	if (!(intent.getExtras().get(Intents.KEY_REQUEST_TYPE) instanceof CAL_REQUEST_TYPE))
	  return false;
	if (!intent.getAction().equals(Intents.ACTION_CALENDAR_REQUEST))
	  return false;

	// If all checks are positive the intent is valid for this class
	return true;
  }

}
