package sabel.meeting_guard.main;

import java.util.logging.Logger;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ToggleButton;
import sabel.meeting_guard.R;
import sabel.meeting_guard.util.Intents;
import sabel.meeting_guard.util.PreferencesResolver;
import sabel.meeting_guard.util.ScreenManager.FRAGMENT;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class StatusTab extends Fragment implements OnClickListener {

  protected Logger log = Logger.getLogger(getClass().getSimpleName());

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	View view = inflater.inflate(R.layout.status_tab, container, false);

	view.findViewById(R.id.status_tab_button_settings).setOnClickListener(this);
	view.findViewById(R.id.status_tab_button_on_off).setOnClickListener(this);
	view.findViewById(R.id.status_tab_image_status).setOnClickListener(this);

	ToggleButton onoff = (ToggleButton) view.findViewById(R.id.status_tab_button_on_off);
	onoff.setChecked(PreferencesResolver.isEnabled(view.getContext()));

	updateStatusIcon(view);

	return view;
  }

  @Override
  public void onClick(View view) {
	int id = view.getId();

	switch (id) {
	case R.id.status_tab_button_on_off:
	  PreferencesResolver.toggleEnabledDisabled(view.getContext());
	  updateStatusIcon();
	  break;
	case R.id.status_tab_button_settings:
	  Intent fragmentIntent = new Intent(Intents.ACTION_SWITCH_FRAGMENT);
	  fragmentIntent.putExtra(Intents.KEY_FRAGMENT_TYPE, FRAGMENT.SETTINGS);
	  view.getContext().sendBroadcast(fragmentIntent);
	  break;
	default:
	  log.warning("View with id " + view.getId() + " is not defined in click listener");
	  break;
	}
  }

  @Override
  public void onResume() {
	super.onResume();
	((Activity) getView().getContext()).getActionBar().setNavigationMode(
		ActionBar.NAVIGATION_MODE_TABS);

	ToggleButton onoff = (ToggleButton) getView().findViewById(R.id.status_tab_button_on_off);
	onoff.setChecked(PreferencesResolver.isEnabled(getView().getContext()));

  }

  /**
   * Update the status icon.
   * 
   * @param context
   */
  private void updateStatusIcon(View view) {
	
	boolean enabled = PreferencesResolver.isEnabled(view.getContext());
	ImageView image = (ImageView) view.findViewById(R.id.status_tab_image_status);
	if (enabled) {
	  // TODO: Find out if watching or Guarding
	  image.setImageResource(R.drawable.full_watching);
	}
	else {
	  image.setImageResource(R.drawable.full_disabled);
	}
  }

  /**
   * Only call this if getView() is not null.
   */
  private void updateStatusIcon() {
	updateStatusIcon(getView());
  }

}
