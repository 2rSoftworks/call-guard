package sabel.meeting_guard.main;

import java.util.logging.Logger;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import sabel.meeting_guard.R;
import sabel.meeting_guard.db.CalendarInterface;
import sabel.meeting_guard.db.DatabaseHandler;
import sabel.meeting_guard.service.Guardian;
import sabel.meeting_guard.util.Intents;
import sabel.meeting_guard.util.ScreenManager;
import sabel.meeting_guard.util.ScreenManager.FRAGMENT;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 * @author Leander
 * 
 */
public class GuardMain extends Activity {

  protected Logger log = Logger.getLogger(getClass().getSimpleName());

  private ScreenManager screenManager;
  private IntentFilter screenManagerIntentFilter;
  private IntentFilter databaseIntentFilter;
  private Intent guardian;

  private DatabaseHandler database;

  private IntentFilter calenderRequestFilter;

  private CalendarInterface calendar;

  @Override
  protected void onCreate(Bundle savedInstanceState) {

	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_guard_main);

	// Create the action bar and set it to tabs mode
	final ActionBar actionBar = getActionBar();
	actionBar.setHomeButtonEnabled(false);
	actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	initializeTabs(actionBar);

	// Set up the Fragment and ScreenManager
	screenManager = new ScreenManager(this);
	screenManagerIntentFilter = new IntentFilter(Intents.ACTION_SWITCH_FRAGMENT);
	this.registerReceiver(screenManager, screenManagerIntentFilter);

	// Request the initial screen from the screenManager
	Intent fragmentIntent = new Intent(Intents.ACTION_SWITCH_FRAGMENT);
	fragmentIntent.putExtra(Intents.KEY_FRAGMENT_TYPE, FRAGMENT.STATUS);
	sendBroadcast(fragmentIntent);

	// Return user to the status tab when he/she clicks on the ActionBar icon
	if (findViewById(android.R.id.home) != null)
	  findViewById(android.R.id.home).setOnClickListener(returnHomeListener);
	if (findViewById(android.R.id.title) != null)
	  findViewById(android.R.id.title).setOnClickListener(returnHomeListener);

	// Setup the database
	database = new DatabaseHandler(this);
	databaseIntentFilter = new IntentFilter(Intents.ACTION_DATABASE_REQUEST);
	registerReceiver(database, databaseIntentFilter);

	// Start the background service that monitors the incoming calls
	guardian = new Intent(this, Guardian.class);
	startService(guardian);
	
	// Setup the calendar interface
	calendar = new CalendarInterface(this);
	calenderRequestFilter = new IntentFilter(Intents.ACTION_CALENDAR_REQUEST);
	registerReceiver(calendar, calenderRequestFilter);

  }

  @Override
  protected void onPause() {
	super.onPause();
	unregisterReceiver(screenManager);
	unregisterReceiver(database);
	unregisterReceiver(calendar);
  }

  @Override
  protected void onResume() {
	super.onResume();
	registerReceiver(screenManager, screenManagerIntentFilter);
	registerReceiver(database, databaseIntentFilter);
	registerReceiver(calendar, calenderRequestFilter);
  }

  /*
   * ######################## Tabs ########################
   */

  /**
   * Add all tabs to the list of tabs and
   * 
   * @param actionBar
   */
  private void initializeTabs(ActionBar actionBar) {
	actionBar.addTab(actionBar.newTab()
		.setText(getString(R.string.tab_name_status))
		.setTabListener(tabListener).setTag(ScreenManager.FRAGMENT.STATUS));
	actionBar.addTab(actionBar.newTab()
		.setText(getString(R.string.tab_name_call_log))
		.setTabListener(tabListener).setTag(ScreenManager.FRAGMENT.LOG));
  }

  ActionBar.TabListener tabListener = new TabListener() {

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
	  Intent fragmentIntent = new Intent(Intents.ACTION_SWITCH_FRAGMENT);
	  fragmentIntent.putExtra(Intents.KEY_FRAGMENT_TYPE,
		  (FRAGMENT) tab.getTag());
	  sendBroadcast(fragmentIntent);
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	  // Unused
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	  // Unused
	}
  };

  @Override
  public void onBackPressed() {

	FRAGMENT currentFragment = screenManager.getCurrentState();

	Intent fragmentIntent = new Intent(Intents.ACTION_SWITCH_FRAGMENT);

	if (currentFragment == FRAGMENT.SETTINGS) {
	  fragmentIntent.putExtra(Intents.KEY_FRAGMENT_TYPE, FRAGMENT.STATUS);
	  sendBroadcast(fragmentIntent);
	}
	else if (currentFragment == FRAGMENT.LOG) {
	  getActionBar().selectTab(getActionBar().getTabAt(0));
	  sendBroadcast(fragmentIntent);
	}
	else if (currentFragment == FRAGMENT.STATUS) {
	  super.onBackPressed();
	}
  }

  private OnClickListener returnHomeListener = new OnClickListener() {
	@Override
	public void onClick(View v) {
	  getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	  getActionBar().selectTab(getActionBar().getTabAt(0));
	}
  };
}
