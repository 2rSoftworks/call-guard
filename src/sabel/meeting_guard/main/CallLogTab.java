package sabel.meeting_guard.main;

import java.util.ArrayList;
import java.util.logging.Logger;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import sabel.meeting_guard.R;
import sabel.meeting_guard.components.CallLogAdapter;
import sabel.meeting_guard.datatypes.Call;
import sabel.meeting_guard.db.DatabaseHandler;
import sabel.meeting_guard.db.DatabaseHandler.DB_RESPONSE_TYPE;
import sabel.meeting_guard.util.Intents;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class CallLogTab extends Fragment {

  protected Logger log = Logger.getLogger(getClass().getSimpleName());

  private IntentFilter database_response;
  private Context context;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
	  Bundle savedInstanceState) {
	View view = inflater.inflate(R.layout.call_log_tab, container, false);
	context = view.getContext();

	database_response = new IntentFilter(Intents.ACTION_DATABASE_RESPONSE);
	view.getContext().registerReceiver(databaseResponseReceiver,
		database_response);

	updateCallLogData();

	// The button should clear the list
	view.findViewById(R.id.call_log_tab_button_clear).setOnClickListener(
		new OnClickListener() {
		  @Override
		  public void onClick(View v) {
			clearList();
			updateCallLogData();
		  }
		});

	return view;
  }

  @Override
  public void onResume() {
	super.onResume();
	((Activity) getView().getContext()).getActionBar().setNavigationMode(
		ActionBar.NAVIGATION_MODE_TABS);
	getView().getContext().registerReceiver(databaseResponseReceiver,
		database_response);
	updateCallLogData();
  }

  @Override
  public void onPause() {
	super.onPause();
	getView().getContext().unregisterReceiver(databaseResponseReceiver);
  }

  /**
   * Request new information from the database.
   */
  private void updateCallLogData() {
	Intent dataRequestIntent = new Intent(Intents.ACTION_DATABASE_REQUEST);
	dataRequestIntent.putExtra(Intents.KEY_REQUEST_TYPE,
		DatabaseHandler.DB_REQUEST_TYPE.GET_ALL);
	context.sendBroadcast(dataRequestIntent);
  }

  /**
   * Request deletion of all calls in the list.
   */
  private void clearList() {
	Intent clearList = new Intent(Intents.ACTION_DATABASE_REQUEST);
	clearList.putExtra(Intents.KEY_REQUEST_TYPE,
		DatabaseHandler.DB_REQUEST_TYPE.REMOVE_ALL);
	context.sendBroadcast(clearList);
  }

  BroadcastReceiver databaseResponseReceiver = new BroadcastReceiver() {

	@Override
	public void onReceive(Context context, Intent intent) {
	  if (isValidIntent(intent)) {
		Bundle extras = intent.getExtras();

		DB_RESPONSE_TYPE type = (DB_RESPONSE_TYPE) extras
			.get(Intents.KEY_RESPONSE_TYPE);

		if (type == DB_RESPONSE_TYPE.CALL_LOG) {
		  ArrayList<Call> calls = convert(extras
			  .get(Intents.KEY_RESPONSE_CONTENT));

		  CallLogAdapter callAdapter = new CallLogAdapter();
		  callAdapter.setCalls(calls);

		  ListView list = (ListView) getView().findViewById(
			  R.id.call_log_tab_list);
		  list.setAdapter(callAdapter);

		}
	  }
	  else {
		log.info("Received invalid intent in CallLogTab");
	  }
	}

	/**
	 * Validate that the intents extras and action are correct.
	 * 
	 * @param intent
	 * @return
	 */
	private boolean isValidIntent(Intent intent) {
	  if (intent.getExtras() == null)
		return false;
	  if (!intent.getExtras().containsKey(Intents.KEY_RESPONSE_TYPE))
		return false;
	  if (!(intent.getExtras().get(Intents.KEY_RESPONSE_TYPE) instanceof DB_RESPONSE_TYPE))
		return false;
	  if (!(intent.getExtras().containsKey(Intents.KEY_RESPONSE_CONTENT)))
		return false;
	  if (!intent.getAction().equals(Intents.ACTION_DATABASE_RESPONSE))
		return false;

	  // If all checks are positive the intent is valid for this class
	  return true;
	}
  };

  /**
   * Convert an Object received from an Intent to an ArrayList<Call> where
   * possible. The list may be empty.
   * 
   * @param inc
   * @return
   */
  private ArrayList<Call> convert(Object intentObject) {
	ArrayList<Call> out = new ArrayList<Call>();

	if (intentObject instanceof ArrayList<?>) {
	  ArrayList<?> inc = (ArrayList<?>) intentObject;

	  for (Object obj : inc) {
		if (obj instanceof Call)
		  out.add((Call) obj);
	  }
	}
	return out;
  }
}
