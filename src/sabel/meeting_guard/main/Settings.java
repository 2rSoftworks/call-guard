package sabel.meeting_guard.main;

import java.util.logging.Logger;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import sabel.meeting_guard.R;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This class builds the preferences screen.
 * 
 * @author Leander
 * 
 */
public class Settings extends PreferenceFragment {

  protected Logger log = Logger.getLogger(getClass().getSimpleName());

  @Override
  public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	addPreferencesFromResource(R.xml.preferences);
  }
 
  @Override
  public void onResume() {
	super.onResume();
//	((Activity) getView().getContext()).getActionBar().hide();
	((Activity) getView().getContext()).getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

  }

}
