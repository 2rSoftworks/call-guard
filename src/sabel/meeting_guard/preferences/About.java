package sabel.meeting_guard.preferences;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebView;
import sabel.meeting_guard.R;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class About extends DialogPreference {

  public About(Context context, AttributeSet attrs) {
	super(context, attrs);
	setDialogLayoutResource(R.layout.preference_about);
  }

  public About(Context context, AttributeSet attrs, int defStyle) {
	super(context, attrs);
	setDialogLayoutResource(R.layout.preference_about);
  }
  
  /** Hide the cancel button */
  @Override
  protected void onPrepareDialogBuilder(Builder builder) {
      super.onPrepareDialogBuilder(builder);
      builder.setNegativeButton(null, null);
  }
  
  @Override
  protected View onCreateDialogView() {
    View view = super.onCreateDialogView();
    
    WebView webView = (WebView) view.findViewById(R.id.about_web_view);
    webView.loadUrl("file:///android_res/raw/about.html");
    
    return view;
  }

}
