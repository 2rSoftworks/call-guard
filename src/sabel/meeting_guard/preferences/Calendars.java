package sabel.meeting_guard.preferences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Logger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import sabel.meeting_guard.R;
import sabel.meeting_guard.datatypes.Calendar;
import sabel.meeting_guard.db.CalendarInterface.CAL_REQUEST_TYPE;
import sabel.meeting_guard.db.CalendarInterface.CAL_RESPONSE_TYPE;
import sabel.meeting_guard.util.Intents;
import sabel.meeting_guard.util.GeneralUtil;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Calendars extends DialogPreference {

  private IntentFilter calenderResponseFilter;
  private LinearLayout layout;
  private HashMap<Calendar, CheckBox> calendarMap;

  protected Logger log = Logger.getLogger(getClass().getSimpleName());

  public Calendars(Context context, AttributeSet attrs) {
	super(context, attrs);
	calendarMap = new HashMap<Calendar, CheckBox>();
  }

  @Override
  public View onCreateDialogView() {

	calenderResponseFilter = new IntentFilter(Intents.ACTION_CALENDAR_RESPONSE);
	getContext().registerReceiver(calendarReceiver, calenderResponseFilter);

	layout = new LinearLayout(getContext());
	layout.setOrientation(LinearLayout.VERTICAL);

	LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
		LinearLayout.LayoutParams.MATCH_PARENT,
		LinearLayout.LayoutParams.WRAP_CONTENT);

	layoutParams.setMargins(30, 20, 30, 0);

	Intent update = new Intent(Intents.ACTION_CALENDAR_REQUEST);
	update.putExtra(Intents.KEY_REQUEST_TYPE, CAL_REQUEST_TYPE.UPDATE);
	getContext().sendBroadcast(update);

	Intent pull = new Intent(Intents.ACTION_CALENDAR_REQUEST);
	pull.putExtra(Intents.KEY_REQUEST_TYPE, CAL_REQUEST_TYPE.GET_CALENDARS);
	getContext().sendBroadcast(pull);

	return layout;
  }

  protected void onDialogClosed(boolean positiveResult) {
	super.onDialogClosed(positiveResult);

	log.info("Positive result: " + positiveResult);

	ArrayList<Integer> checkedCalIds = new ArrayList<Integer>();
	for (Calendar cal : calendarMap.keySet()) {
	  if (calendarMap.get(cal).isChecked()) {
		checkedCalIds.add((int) cal.getId());
	  }
	}

	// Save the selected calendars as a string
	SharedPreferences prefs = PreferenceManager
		.getDefaultSharedPreferences(getContext());
	Editor prefEditor = prefs.edit();
	prefEditor.putString(getContext().getString(R.string.key_dialog_calendar),
		checkedCalIds.toString());
	prefEditor.commit();
	getContext().unregisterReceiver(calendarReceiver);
  }

  BroadcastReceiver calendarReceiver = new BroadcastReceiver() {

	@Override
	public void onReceive(Context context, Intent intent) {

	  Bundle extras = intent.getExtras();
	  CAL_RESPONSE_TYPE type = (CAL_RESPONSE_TYPE) extras
		  .get(Intents.KEY_RESPONSE_TYPE);

	  if (type == CAL_RESPONSE_TYPE.RETURN_CALENDARS) {
		ArrayList<Calendar> calendars = convert(extras
			.get(Intents.KEY_RESPONSE_CONTENT));

		Collections.sort(calendars);
		calendarMap = new HashMap<Calendar, CheckBox>();

		SharedPreferences prefs = PreferenceManager
			.getDefaultSharedPreferences(getContext());
		String values = prefs.getString(
			getContext().getString(R.string.key_dialog_calendar), "");
		Set<Long> checked = GeneralUtil.fromString(values);

		String lastCalenderSource = "[]";
		for (Calendar calendar : calendars) {

		  if (!calendar.getOwnerAccount().equals(lastCalenderSource)) {
			lastCalenderSource = calendar.getOwnerAccount();
			TextView title = new TextView(context);
			title.setText(lastCalenderSource);
			title.setTypeface(Typeface.DEFAULT_BOLD);
			title.setPadding(10, 10, 0, 0);
			layout.addView(title);

			View line = new View(context);
			line.setBackgroundColor(Color.DKGRAY);
			line.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT, 2));
			layout.addView(line);
		  }

		  CheckBox box = new CheckBox(context);
		  box.setText(calendar.getDisplayName());
		  if (checked.contains(calendar.getId()))
			box.setChecked(true);
		  layout.addView(box);

		  // Keep the CheckBox and the calendar in a map to save it later.
		  calendarMap.put(calendar, box);
		}
	  }
	}
  };

  /**
   * Convert an object safely to an ArrayList<Calendar> where possible. May
   * return an empty list.
   * 
   * @param obj
   * @return
   */
  private ArrayList<Calendar> convert(Object obj) {
	ArrayList<Calendar> result = new ArrayList<Calendar>();

	if (obj instanceof ArrayList<?>) {
	  ArrayList<?> generic = (ArrayList<?>) obj;

	  for (Object element : generic) {
		if (element instanceof Calendar)
		  result.add((Calendar) element);
	  }
	}
	return result;
  }

}
