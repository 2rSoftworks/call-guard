package sabel.meeting_guard.preferences;

import java.util.logging.Logger;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import sabel.meeting_guard.R;
import sabel.meeting_guard.service.Guardian;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This class builds the preferences screen.
 * 
 * @author Leander
 * 
 */
public class Settings extends PreferenceFragment implements OnSharedPreferenceChangeListener {

  protected Logger log = Logger.getLogger(getClass().getSimpleName());

  @Override
  public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	addPreferencesFromResource(R.xml.preferences);

  }

  @Override
  public void onResume() {
	super.onResume();
	((Activity) getView().getContext()).getActionBar().setNavigationMode(
		ActionBar.NAVIGATION_MODE_STANDARD);
	getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	updateEnabled();
  }

  @Override
  public void onPause() {
	super.onPause();
	getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
  }

  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
	// Update the UI
	updateEnabled();

	if (key.equals(getString(R.string.key_switch_all))) {
	  boolean enabled = sharedPreferences.getBoolean(key, true);
	  if (enabled) {
		Intent guardian = new Intent(getActivity(), Guardian.class);
		getActivity().startService(guardian);
	  }
	}
  }

  private void updateEnabled() {
	SharedPreferences pref = getPreferenceScreen().getSharedPreferences();
	boolean allEnabled = pref.getBoolean(getString(R.string.key_switch_all), true);
	findPreference(getString(R.string.key_switch_calendar)).setEnabled(allEnabled);
	findPreference(getString(R.string.key_switch_location)).setEnabled(allEnabled);
	findPreference(getString(R.string.key_dialog_calendar)).setEnabled(allEnabled);
	findPreference(getString(R.string.key_dialog_geolocation)).setEnabled(allEnabled);
	findPreference(getString(R.string.key_switch_sms_response)).setEnabled(allEnabled);
	findPreference(getString(R.string.key_edit_sms_text)).setEnabled(allEnabled);

	if (allEnabled) {
	  boolean calEnabled = pref.getBoolean(getString(R.string.key_switch_calendar), true);
	  findPreference(getString(R.string.key_dialog_calendar)).setEnabled(calEnabled);

	  boolean geoEnabled = pref.getBoolean(getString(R.string.key_switch_location), true);
	  findPreference(getString(R.string.key_dialog_geolocation)).setEnabled(geoEnabled);

	  boolean smsEnabled = pref.getBoolean(getString(R.string.key_switch_sms_response), true);
	  findPreference(getString(R.string.key_edit_sms_text)).setEnabled(smsEnabled);
	}

  }

}
