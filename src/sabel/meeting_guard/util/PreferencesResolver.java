package sabel.meeting_guard.util;

import java.util.Set;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import sabel.meeting_guard.R;
import sabel.meeting_guard.service.Guardian;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class PreferencesResolver {

  /**
   * Get the watched calendars from the shared preferences.
   * 
   * @param context
   * @return
   */
  public static Set<Long> getWatchedCalendars(Context context) {
	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
	String calendars = preferences.getString(context.getString(R.string.key_dialog_calendar), "");
	return GeneralUtil.fromString(calendars);
  }

  public static boolean isEnabled(Context context) {
	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
	return preferences.getBoolean(context.getString(R.string.key_switch_all), true);
  }

  public static boolean isCalendarWachtingEnabled(Context context) {
	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
	return preferences.getBoolean(context.getString(R.string.key_switch_calendar), true);
  }

  public static boolean isLocationWachtingEnabled(Context context) {
	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
	return preferences.getBoolean(context.getString(R.string.key_switch_location), true);
  }

  public static void toggleEnabledDisabled(Context context) {
	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
	Editor editor = preferences.edit();
	String key = context.getString(R.string.key_switch_all);
	editor.putBoolean(key, !preferences.getBoolean(key, true));
	editor.commit();

	if (preferences.getBoolean(key, true)) {
	  Intent guardian = new Intent(context, Guardian.class);
	  context.startService(guardian);
	}
  }

  public static boolean isSMSResponseEnabled(Context context) {
	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
	return preferences.getBoolean(context.getString(R.string.key_switch_sms_response), true);
  }

  public static String getSMSResponseText(Context context) {
	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
	return preferences.getString(context.getString(R.string.key_edit_sms_text),
		context.getString(R.string.default_sms_text));
  }
}
