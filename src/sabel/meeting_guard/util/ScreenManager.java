package sabel.meeting_guard.util;

import java.util.HashMap;
import java.util.logging.Logger;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import sabel.meeting_guard.R;
import sabel.meeting_guard.main.CallLogTab;
import sabel.meeting_guard.main.StatusTab;
import sabel.meeting_guard.preferences.Settings;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This class keeps a map of all fragments in the application and can switch
 * between them if requested to do so by an intent.
 * 
 * 
 * EXAMPLE:
 * 
 * Intent intent = new Intent(getString(R.string.intent_switch_fragment));
 * intent.putExtra(getString(R.string.key_fragment_type), FRAGMENT.STATUS);
 * 
 * @author Leander
 * 
 */
public class ScreenManager extends BroadcastReceiver {

  public static enum FRAGMENT {
	STATUS, LOG, SETTINGS
  }

  private FRAGMENT currentState;

  protected Logger log = Logger.getLogger(getClass().getSimpleName());

  private HashMap<FRAGMENT, Fragment> fragments;

  public ScreenManager(Context context) {

	initializeFragments();
  }

  /**
   * @return the currentState
   */
  public FRAGMENT getCurrentState() {
	return currentState;
  }

  /**
   * @param currentState
   *          the currentState to set
   */
  public void setCurrentState(FRAGMENT currentState) {
	this.currentState = currentState;
  }

  /**
   * Validate incoming intent, retrieve all information and replace the current
   * fragment with the requested one.
   */
  @Override
  public void onReceive(Context context, Intent intent) {

	if (isValidIntent(intent)) {
	  Bundle extras = intent.getExtras();
	  FRAGMENT type = (FRAGMENT) extras.get(Intents.KEY_FRAGMENT_TYPE);
	  currentState = type;
	  Fragment fragment = getFragment(type);

	  FragmentManager manager = ((Activity) context).getFragmentManager();
	  FragmentTransaction transaction = manager.beginTransaction();
	  transaction.replace(R.id.activity_guard_main_container, fragment);
	  transaction.commit();
	}
	else {
	  log.warning("An intent of type " + intent.getAction() + " arrived in the " + this.getClass().getSimpleName()
		  + " but could not be processed due to a wrong action or a wrong/missing extra");
	}

  }

  /**
   * Get the fragment for a given FRAGMENT type.
   * 
   * @param tab
   * @return
   */
  private Fragment getFragment(FRAGMENT tab) {
	return fragments.get(tab);
  }

  /**
   * Validate that the intents extras and action are correct.
   * 
   * @param intent
   * @return
   */
  private boolean isValidIntent(Intent intent) {
	if (intent.getExtras() == null)
	  return false;
	if (!intent.getExtras().containsKey(Intents.KEY_FRAGMENT_TYPE))
	  return false;
	if (!(intent.getExtras().get(Intents.KEY_FRAGMENT_TYPE) instanceof FRAGMENT))
	  return false;
	if (!intent.getAction().equals(Intents.ACTION_SWITCH_FRAGMENT))
	  return false;

	// If all checks are positive the intent is valid for this class
	return true;
  }

  /**
   * Initialize all fragments when the application is first started.
   */
  private void initializeFragments() {
	fragments = new HashMap<FRAGMENT, Fragment>();
	fragments.put(FRAGMENT.STATUS, new StatusTab());
	fragments.put(FRAGMENT.LOG, new CallLogTab());
	fragments.put(FRAGMENT.SETTINGS, new Settings());
  }

}
