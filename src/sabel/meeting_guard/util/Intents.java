package sabel.meeting_guard.util;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This class contains all actions and key strings that are needed to send or
 * receive intents.
 * 
 * @author Leander
 * 
 */
public class Intents {

  public static final String ACTION_SWITCH_FRAGMENT = "sabel.meeting_guard.switch.fragment";
  public static final String ACTION_DATABASE_REQUEST = "sabel.meeting_guard.db.request";
  public static final String ACTION_DATABASE_RESPONSE = "sabel.meeting_guard.db.response";
  public static final String ACTION_CALENDAR_REQUEST = "sabel.meeting_guard.cal.request";
  public static final String ACTION_CALENDAR_RESPONSE = "sabel.meeting_guard.cal.response";
  
  public static final String KEY_FRAGMENT_TYPE = "sabel.meeting_guard.fragment_type";
  public static final String KEY_REQUEST_TYPE = "sabel.meeting_guard.request_type";
  public static final String KEY_REQUEST_CONTENT = "sabel.meeting_guard.request_content";
  public static final String KEY_RESPONSE_TYPE = "sabel.meeting_guard.response_type";
  public static final String KEY_RESPONSE_CONTENT = "sabel.meeting_guard.response_content";
  

}
