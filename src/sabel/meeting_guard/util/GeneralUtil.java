package sabel.meeting_guard.util;

import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

import sabel.meeting_guard.datatypes.Event;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class GeneralUtil {

  /**
   * Parse the result of ArrayList<Integer>.toString() as a set of Integers.
   * 
   * @param string
   * @return
   */
  public static Set<Long> fromString(String string) {
	String[] strings = string.replace("[", "").replace("]", "").split(", ");
	Set<Long> result = new HashSet<Long>();

	for (int i = 0; i < strings.length; i++) {
	  if (!strings[i].equals(""))
		result.add(Long.parseLong(strings[i]));
	}
	return result;
  }

  /**
   * Remove all events from the queue that have already passed. After this the first event in the
   * Queue will be the next event in a watched calendar.
   * 
   * @param events
   * @return
   */
  public static PriorityQueue<Event> removePastEvents(PriorityQueue<Event> events) {
	if (events == null || events.size() == 0)
	  return new PriorityQueue<Event>();

	long now = System.currentTimeMillis();
	while (events.peek() != null && events.peek().getEndTime() < now) {
	  events.poll();
	}
	return events;

  }
}
