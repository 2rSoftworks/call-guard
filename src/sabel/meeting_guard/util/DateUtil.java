package sabel.meeting_guard.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.util.Log;

/* 
 * Copyright (c) 2012 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This class provides various utility functions that transfer between dates as {@link String} and
 * {@link Date}. This is necessary because SQLite does not have a date class and all dates must be
 * stored as string.
 * 
 * Additionally it is possible to show or parse date and time values separately.
 * 
 * @author Leander
 * 
 */
public class DateUtil {

  public static long SECOND = 1000;
  public static long MINUTE = 60 * SECOND;
  public static long HOUR = 60 * MINUTE;
  public static long DAY = 24 * HOUR;

  private static DateFormat timeFormat = SimpleDateFormat.getTimeInstance(SimpleDateFormat.SHORT,
	  Locale.getDefault());
  private static DateFormat dateFormat = SimpleDateFormat.getDateInstance(SimpleDateFormat.MEDIUM,
	  Locale.getDefault());
  private static DateFormat timeAndDateFormat = SimpleDateFormat.getDateTimeInstance(
	  SimpleDateFormat.MEDIUM, SimpleDateFormat.SHORT, Locale.getDefault());

  /**
   * This method combines two {@link Date} objects. One containing the relevant time and one
   * containing the relevant date to a single Date object.
   * 
   * This is needed because time and date pickers return these as separate objects.
   * 
   * @param time
   * @param date
   * @return
   */
  public static Date combineTimeAndDate(Date time, Date date) {
	Calendar cTime = Calendar.getInstance();
	Calendar cDate = Calendar.getInstance();
	cTime.setTime(time);
	cDate.setTime(date);

	Calendar dateTime = Calendar.getInstance();
	dateTime.set(Calendar.YEAR, cDate.get(Calendar.YEAR));
	dateTime.set(Calendar.MONTH, cDate.get(Calendar.MONTH));
	dateTime.set(Calendar.DAY_OF_MONTH, cDate.get(Calendar.DAY_OF_MONTH));
	dateTime.set(Calendar.HOUR_OF_DAY, cTime.get(Calendar.HOUR_OF_DAY));
	dateTime.set(Calendar.MINUTE, cTime.get(Calendar.MINUTE));

	return dateTime.getTime();
  }

  /**
   * Parse a sting of the default format as a {@link Date}
   * 
   * @param dateAsString
   * @return
   */
  public static Date parseStringAsDate(String dateAsString) {

	try {
	  return dateFormat.parse(dateAsString);
	}
	catch (ParseException e) {
	  Log.e("ERROR", e.getMessage());
	  e.printStackTrace();
	  return new Date();
	}
  }

  /**
   * Parse a string in the default format as a {@link Date}
   * 
   * @param timeAsString
   * @return
   */
  public static Date parseStringAsTime(String timeAsString) {
	// TODO: Rename
	try {
	  return timeFormat.parse(timeAsString);
	}
	catch (ParseException e) {
	  Log.e("ERROR", e.getMessage());
	  e.printStackTrace();
	  return new Date();
	}
  }

  /**
   * Parse a string in the default format as a date
   * 
   * @param dateAndTime
   * @return
   */
  public static Date parseStringAsBoth(String dateAndTime) {
	if (dateAndTime == null) {
	  return null;
	}
	try {
	  Date timeAndDate = timeAndDateFormat.parse(dateAndTime);
	  return timeAndDate;
	}
	catch (ParseException e) {
	  Log.e("ERROR", e.getMessage());
	  e.printStackTrace();
	  return new Date();
	}
  }

  /**
   * Create a date string in the default format
   * 
   * @param date
   * @return
   */
  public static String dateToString(Date date) {

	return dateFormat.format(date);

  }

  /**
   * Create a time string in the default format
   * 
   * @param time
   * @return
   */
  public static String timeToString(Date time) {
	return timeFormat.format(time);
  }

  /**
   * Parse a date in the default local format
   * 
   * @param timeAndDate
   * @return
   */
  public static String timeAndDateToString(Date timeAndDate) {
	return timeAndDateFormat.format(timeAndDate);
  }

  /**
   * Take the current date from the {@link Calendar} and return it in the format dd.mm.yyy
   * 
   * @return
   */
  public static String currentDateAsString() {
	Date currentDate = new Date(System.currentTimeMillis());
	return dateFormat.format(currentDate);
  }

  /**
   * Take the current time from the {@link Calendar} and return it in the format hh:mm
   * 
   * @return
   */
  public static String currentTimeAsString() {
	Date currentTime = new Date(System.currentTimeMillis());
	return timeFormat.format(currentTime);
  }
}
