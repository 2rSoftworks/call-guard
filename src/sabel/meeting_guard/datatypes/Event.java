package sabel.meeting_guard.datatypes;

import java.io.Serializable;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This class holds information on a single calendar event.
 * 
 * @author Leander
 * 
 */
public class Event implements Comparable<Event>, Serializable {

  private static final long serialVersionUID = -5615613345108854537L;

  private long eventId;
  private long calId;
  private int availability;
  private long startTime;
  private long endTime;
  private String title;

  /**
   * @param eventId
   * @param calId
   * @param availability
   * @param starTime
   * @param duration
   */
  public Event(long eventId, long calId, int availability, long starTime, long duration,
	  String title) {
	super();
	this.eventId = eventId;
	this.calId = calId;
	this.availability = availability;
	this.startTime = starTime;
	this.endTime = duration;
	this.title = title;
  }

  /**
   * @return the eventId
   */
  public long getEventId() {
	return eventId;
  }

  /**
   * @param eventId
   *          the eventId to set
   */
  public void setEventId(long eventId) {
	this.eventId = eventId;
  }

  /**
   * @return the calId
   */
  public long getCalId() {
	return calId;
  }

  /**
   * @param calId
   *          the calId to set
   */
  public void setCalId(long calId) {
	this.calId = calId;
  }

  /**
   * @return the availability
   */
  public int getAvailability() {
	return availability;
  }

  /**
   * @param availability
   *          the availability to set
   */
  public void setAvailability(int availability) {
	this.availability = availability;
  }

  /**
   * @return the starTime
   */
  public long getStartTime() {
	return startTime;
  }

  /**
   * @param starTime
   *          the starTime to set
   */
  public void setStarTime(long starTime) {
	this.startTime = starTime;
  }

  /**
   * @return the duration
   */
  public long getEndTime() {
	return endTime;
  }

  /**
   * @param endTime
   *          the duration to set
   */
  public void setEndTime(long endTime) {
	this.endTime = endTime;
  }

  /**
   * @return the title
   */
  public String getTitle() {
	return title;
  }

  /**
   * @param title
   *          the title to set
   */
  public void setTitle(String title) {
	this.title = title;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + availability;
	result = prime * result + (int) (calId ^ (calId >>> 32));
	result = prime * result + (int) (endTime ^ (endTime >>> 32));
	result = prime * result + (int) (eventId ^ (eventId >>> 32));
	result = prime * result + (int) (startTime ^ (startTime >>> 32));
	return result;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
	if (this == obj)
	  return true;
	if (obj == null)
	  return false;
	if (!(obj instanceof Event))
	  return false;
	Event other = (Event) obj;
	if (availability != other.availability)
	  return false;
	if (calId != other.calId)
	  return false;
	if (endTime != other.endTime)
	  return false;
	if (eventId != other.eventId)
	  return false;
	if (startTime != other.startTime)
	  return false;
	return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("Event [eventId=");
	builder.append(eventId);
	builder.append(", calId=");
	builder.append(calId);
	builder.append(", availability=");
	builder.append(availability);
	builder.append(", startTime=");
	builder.append(startTime);
	builder.append(", endTime=");
	builder.append(endTime);
	builder.append(", title=");
	builder.append(title);
	builder.append("]");
	return builder.toString();
  }

  /**
   * Compare the start time of two events for sorting.
   * 
   * @param another
   */
  @Override
  public int compareTo(Event another) {
	Long start = startTime;
	return start.compareTo(another.getStartTime());
  }

}
