package sabel.meeting_guard.datatypes;

import java.io.Serializable;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This contains information about a specific calendar.
 * 
 * It can be sorted by owner account.
 * 
 * @author Leander
 * 
 */
public class Calendar implements Serializable, Comparable<Calendar> {

  private static final long serialVersionUID = 2091329676586688760L;

  private long id;
  private String accountName;
  private String displayName;
  private String ownerAccount;

  /**
   * @param id
   * @param accountName
   * @param displayName
   * @param ownerAccount
   */
  public Calendar(long id, String accountName, String displayName,
	  String ownerAccount) {
	super();
	this.id = id;
	this.accountName = accountName;
	this.displayName = displayName;
	this.ownerAccount = ownerAccount;
  }

  /**
   * @return the id
   */
  public long getId() {
	return id;
  }

  /**
   * @param id
   *          the id to set
   */
  public void setId(int id) {
	this.id = id;
  }

  /**
   * @return the accountName
   */
  public String getAccountName() {
	return accountName;
  }

  /**
   * @param accountName
   *          the accountName to set
   */
  public void setAccountName(String accountName) {
	this.accountName = accountName;
  }

  /**
   * @return the displayName
   */
  public String getDisplayName() {
	return displayName;
  }

  /**
   * @param displayName
   *          the displayName to set
   */
  public void setDisplayName(String displayName) {
	this.displayName = displayName;
  }

  /**
   * @return the ownerAccount
   */
  public String getOwnerAccount() {
	return ownerAccount;
  }

  /**
   * @param ownerAccount
   *          the ownerAccount to set
   */
  public void setOwnerAccount(String ownerAccount) {
	this.ownerAccount = ownerAccount;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("Calendar [id=");
	builder.append(id);
	builder.append(", accountName=");
	builder.append(accountName);
	builder.append(", displayName=");
	builder.append(displayName);
	builder.append(", ownerAccount=");
	builder.append(ownerAccount);
	builder.append("]");
	return builder.toString();
  }

  @Override
  public int compareTo(Calendar another) {
	return ownerAccount.compareTo(another.ownerAccount);
  }

}
