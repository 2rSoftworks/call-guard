package sabel.meeting_guard.datatypes;

import java.io.Serializable;
import java.util.Date;

import android.database.Cursor;
import sabel.meeting_guard.db.TableCalls;
import sabel.meeting_guard.util.DateUtil;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Call implements Serializable {

  private static final long serialVersionUID = 5571063586934731102L;

  private int id;
  private String number;
  private Date date;
  private String action;

  /**
   * 
   * @param number
   * @param date
   * @param action
   */
  public Call(String number, Date date, String action) {
	super();
	this.number = number;
	this.date = date;
	this.action = action;
  }

  /**
   * 
   * @param id
   * @param number
   * @param date
   * @param action
   */
  private Call(int id, String number, Date date, String action) {
	super();
	this.id = id;
	this.number = number;
	this.date = date;
	this.action = action;
  }

  /**
   * Create a new Call from an SQL cursor
   * 
   * @param cursor
   * @return
   */
  public static Call CallFromCursor(Cursor cursor) {
	int id = cursor.getInt(TableCalls.ID.getColumn());
	String number = cursor.getString(TableCalls.NUMBER.getColumn());
	Date time = DateUtil.parseStringAsBoth(cursor.getString(TableCalls.TIME.getColumn()));
	String action = cursor.getString(TableCalls.ACTION.getColumn());
	return new Call(id, number, time, action);
  }

  /**
   * @return the id
   */
  public int getId() {
	return id;
  }

  /**
   * @param id
   *          the id to set
   */
  public void setId(int id) {
	this.id = id;
  }

  /**
   * @return the number
   */
  public String getNumber() {
	return number;
  }

  /**
   * @param number
   *          the number to set
   */
  public void setNumber(String number) {
	this.number = number;
  }

  /**
   * @return the date
   */
  public Date getDate() {
	return date;
  }

  /**
   * @param date
   *          the date to set
   */
  public void setDate(Date date) {
	this.date = date;
  }

  /**
   * @return the action
   */
  public String getAction() {
	return action;
  }

  /**
   * @param action
   *          the action to set
   */
  public void setAction(String action) {
	this.action = action;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("Call [id=");
	builder.append(id);
	builder.append(", number=");
	builder.append(number);
	builder.append(", date=");
	builder.append(date);
	builder.append(", action=");
	builder.append(action);
	builder.append("]");
	return builder.toString();
  }

}
