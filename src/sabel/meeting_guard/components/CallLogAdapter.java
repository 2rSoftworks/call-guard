package sabel.meeting_guard.components;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sabel.meeting_guard.R;
import sabel.meeting_guard.datatypes.Call;
import sabel.meeting_guard.util.DateUtil;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class CallLogAdapter extends BaseAdapter {

  private ArrayList<Call> calls = new ArrayList<Call>();

  @Override
  public View getView(int position, View row, ViewGroup parent) {

	LogHolder holder = null;

	if (row == null) {
	  Context context = parent.getContext();
	  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	  row = inflater.inflate(R.layout.call_log_row, null);

	  holder = new LogHolder();
	  holder.position = position;
	  holder.number = (TextView) row.findViewById(R.id.call_log_row_label_number);
	  holder.time = (TextView) row.findViewById(R.id.call_log_row_label_time);
	  holder.action = (TextView) row.findViewById(R.id.call_log_row_label_action);
	  row.setTag(holder);

	  row.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View view) {
		  int position = ((LogHolder) view.getTag()).position;
		  Call call = calls.get(position);

		  Uri number = Uri.parse("tel:" + call.getNumber());
		  Intent dial = new Intent(Intent.ACTION_DIAL);
		  dial.setData(number);
		  view.getContext().startActivity(dial);
		}
	  });

	}
	else {
	  holder = (LogHolder) row.getTag();
	}

	// Set the correct values
	holder.number.setText(calls.get(position).getNumber());
	holder.time.setText(DateUtil.timeAndDateToString(calls.get(position).getDate()));
	holder.action.setText(calls.get(position).getAction());

	return row;
  }

  /**
   * @param calls
   *          the calls to set
   */
  public void setCalls(ArrayList<Call> calls) {
	this.calls = calls;
  }

  @Override
  public int getCount() {
	return calls.size();
  }

  @Override
  public Object getItem(int position) {
	return calls.get(position);
  }

  @Override
  public long getItemId(int position) {
	// TODO Auto-generated method stub
	return calls.get(position).getId();
  }

  public static class LogHolder {
	public int position;
	public TextView number;
	public TextView time;
	public TextView action;
  }
}
