package sabel.meeting_guard.service;

import java.util.PriorityQueue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import sabel.meeting_guard.R;
import sabel.meeting_guard.datatypes.Event;
import sabel.meeting_guard.db.CalendarInterface;
import sabel.meeting_guard.db.CalendarInterface.CAL_RESPONSE_TYPE;
import sabel.meeting_guard.main.GuardMain;
import sabel.meeting_guard.util.DateUtil;
import sabel.meeting_guard.util.GeneralUtil;
import sabel.meeting_guard.util.Intents;
import sabel.meeting_guard.util.PreferencesResolver;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the background service that monitors current calendar entries.
 * 
 * @author Leander
 * 
 */
public class Guardian extends IntentService {

  protected Logger log = Logger.getLogger(getClass().getSimpleName());

  public enum STATUS {
	GUARDING, WATCHING, DISABLED
  }

  private STATUS currentState;
  private CallReceiver callReceiver;
  private IntentFilter phoneFilter;
  private IntentFilter calenderFilter;
  private boolean registered;

  private PriorityQueue<Event> events;

  public Guardian() {
	super(Guardian.class.getSimpleName());

	callReceiver = new CallReceiver(this);
	phoneFilter = new IntentFilter(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
	calenderFilter = new IntentFilter(Intents.ACTION_CALENDAR_RESPONSE);

	events = new PriorityQueue<Event>();

	log.info("Guardian service created.");
  }

  @Override
  protected void onHandleIntent(Intent intent) {
	log.info("Guardian service started");

	activateGuardian();
	update();

	// Listen for responses from the database
	registerReceiver(calenderReceiver, calenderFilter);

	Timer bigUpdateTimer = new Timer();
	bigUpdateTimer.scheduleAtFixedRate(bigUpdate, 0, DateUtil.MINUTE);

	while (currentState == STATUS.GUARDING || currentState == STATUS.WATCHING) {
	  try {
		update();
		Thread.sleep(5 * DateUtil.SECOND);
	  }
	  catch (InterruptedException e) {
		log.severe("Could not sleep in Guardian Service");
	  }
	}
  }

  private void update() {

	// Remove events that have passed from the list
	events = GeneralUtil.removePastEvents(events);

	if (!PreferencesResolver.isEnabled(this)) {
	  currentState = STATUS.DISABLED;
	  deactivateGuardian();
	  stopSelf();
	}
	else if (events.isEmpty()) {
	  currentState = STATUS.WATCHING;
	  deactivateGuardian();
	}
	else if (events.peek().getStartTime() < System.currentTimeMillis()) {
	  currentState = STATUS.GUARDING;
	  activateGuardian();
	}
  }

  TimerTask bigUpdate = new TimerTask() {

	@Override
	public void run() {
	  log.info("Performing big update");

	  // Get the first update from the database
	  Intent request = new Intent(Intents.ACTION_CALENDAR_REQUEST);
	  request.putExtra(Intents.KEY_REQUEST_TYPE, CalendarInterface.CAL_REQUEST_TYPE.GET_EVENTS);
	  request.putExtra(Intents.KEY_REQUEST_CONTENT, DateUtil.DAY);
	  sendBroadcast(request);

	  events = GeneralUtil.removePastEvents(events);
	}
  };

  /**
   * Update the notification based on the current state of the process.
   */
  private void updateNotification() {

	if (currentState == STATUS.DISABLED) {
	  stopForeground(true);
	  return;
	}

	String title = "";
	String body = "";
	Integer icon = 0;
	if (currentState == STATUS.GUARDING) {
	  title = getString(R.string.notification_title_active);
	  body = getString(R.string.notification_body_active);
	  icon = R.drawable.notification_guarding;
	}
	else if (currentState == STATUS.WATCHING) {
	  title = getString(R.string.notification_title_enabled);
	  body = getString(R.string.notification_body_enabled);
	  icon = R.drawable.notification_watching;
	}

	Intent intent = new Intent(this, GuardMain.class);
	PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 01, intent,
		Intent.FLAG_ACTIVITY_CLEAR_TASK);
	Notification.Builder builder = new Notification.Builder(getBaseContext());
	builder.setSmallIcon(icon).setContentTitle(title).setContentText(body)
		.setContentIntent(pendingIntent).setOngoing(true);
	startForeground(Notification.FLAG_FOREGROUND_SERVICE, builder.build());

  }

  /** ----------- Broadcast receiver ----------- **/

  // Receive calendar information
  BroadcastReceiver calenderReceiver = new BroadcastReceiver() {

	@Override
	public void onReceive(Context context, Intent intent) {

	  if (isValidCalendarIntent(intent)) {
		Bundle extras = intent.getExtras();
		CAL_RESPONSE_TYPE type = (CAL_RESPONSE_TYPE) extras.get(Intents.KEY_RESPONSE_TYPE);

		if (type == CAL_RESPONSE_TYPE.RETURN_EVENTS) {
		  events = convert(extras.get(Intents.KEY_RESPONSE_CONTENT));
		  events = GeneralUtil.removePastEvents(events);
		}
		else {
		  log.info("Discarding intent of wrong type: " + type + " in " + getClass().getSimpleName());
		}
	  }
	  else {
		log.warning("Received invalid calendar intent in " + getClass().getSimpleName());
	  }

	}

  };

  // Receive information about settings updates
  BroadcastReceiver settingsUpdateReceiver = new BroadcastReceiver() {

	@Override
	public void onReceive(Context context, Intent intent) {
	  // TODO Auto-generated method stub

	}
  };

  /** ----------- Public getter and setter ----------- **/

  /**
   * @return the currentState
   */
  public STATUS getCurrentState() {
	return currentState;
  }

  @Override
  public void onDestroy() {
	super.onDestroy();
	deactivateGuardian();
	unregisterReceiver(calenderReceiver);
	log.info(getClass().getSimpleName() + " destroyed");
  }

  /** ----------- Private helper functions ----------- **/

  /**
   * Start monitoring incoming calls.
   */
  private void activateGuardian() {
	if (!registered) {
	  registerReceiver(callReceiver, phoneFilter);
	  registered = true;
	}
	updateNotification();

  }

  /**
   * Stop monitoring incoming calls.
   */
  private void deactivateGuardian() {
	if (registered) {
	  unregisterReceiver(callReceiver);
	  registered = false;
	}
	updateNotification();
  }

  /**
   * Safely convert the PriorityQueue<Event> received trough an intent from a generic object.
   * 
   * @param obj
   * @return Result may be empty but not null.
   */
  private PriorityQueue<Event> convert(Object obj) {
	PriorityQueue<Event> result = new PriorityQueue<Event>();
	if (obj instanceof PriorityQueue<?>) {
	  PriorityQueue<?> q = (PriorityQueue<?>) obj;
	  while (q.peek() != null) {
		Object cur = q.poll();
		if (cur instanceof Event)
		  result.add((Event) cur);
	  }
	}
	return result;
  }

  private boolean isValidCalendarIntent(Intent intent) {
	if (intent.getExtras() == null)
	  return false;
	if (!intent.getExtras().containsKey(Intents.KEY_RESPONSE_TYPE))
	  return false;
	if (!(intent.getExtras().get(Intents.KEY_RESPONSE_TYPE) instanceof CAL_RESPONSE_TYPE))
	  return false;
	if (!intent.getAction().equals(Intents.ACTION_CALENDAR_RESPONSE))
	  return false;

	// If all checks are positive the intent is valid for this class
	return true;

  }

}
