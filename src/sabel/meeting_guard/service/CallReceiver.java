package sabel.meeting_guard.service;

import java.util.Date;
import java.util.logging.Logger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import sabel.meeting_guard.datatypes.Call;
import sabel.meeting_guard.db.DatabaseHandler;
import sabel.meeting_guard.service.Guardian.STATUS;
import sabel.meeting_guard.util.Intents;
import sabel.meeting_guard.util.PreferencesResolver;

/* 
 * Copyright (c) 2013 Leander Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This call receiver is registered in the applications manifest and can receive incoming calls.
 * Based on the application state the calls will be allowed or muted and written to the database.
 * 
 * @author Leander
 * 
 */
public class CallReceiver extends BroadcastReceiver {

  protected Logger log = Logger.getLogger(getClass().getSimpleName());
  private TelephonyManager telephone;
  private Context context;
  private Guardian guard;

  public CallReceiver(Guardian guard) {
	// Keep a reference to the Guardian service to find out if calls should be muted or not.
	this.guard = guard;
  }

  @Override
  public void onReceive(Context context, Intent intent) {
	log.info("Listening to the phone state");
	this.context = context;
	telephone = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
	telephone.listen(phoneState, PhoneStateListener.LISTEN_CALL_STATE);
  }

  public void onDestroy() {
	log.info("No longer listening to the phone state");
	telephone.listen(null, PhoneStateListener.LISTEN_NONE);
  }

  PhoneStateListener phoneState = new PhoneStateListener() {

	@Override
	public void onCallStateChanged(int state, String incomingNumber) {
	  // The audio manager is able to manipulate the phones ring tone volume
	  AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

	  switch (state) {
	  case TelephonyManager.CALL_STATE_IDLE:
		// Put the phone back into normal mode
		audio.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
		break;
	  case TelephonyManager.CALL_STATE_OFFHOOK:
		// Not really interested in this
		break;
	  case TelephonyManager.CALL_STATE_RINGING:
		if (guard.getCurrentState() == STATUS.GUARDING) {

		  // Mute the call
		  audio.setRingerMode(AudioManager.RINGER_MODE_SILENT);
		  log.info("Incoming call from " + incomingNumber + " was muted");

		  // Save the call details to the call log database
		  Call call = new Call(incomingNumber, new Date(System.currentTimeMillis()), "Supressed");
		  Intent callIntent = new Intent(Intents.ACTION_DATABASE_REQUEST);
		  callIntent.putExtra(Intents.KEY_REQUEST_TYPE, DatabaseHandler.DB_REQUEST_TYPE.ADD_CALL);
		  callIntent.putExtra(Intents.KEY_REQUEST_CONTENT, call);
		  context.sendBroadcast(callIntent);

		  // Reply with an SMS to the caller
		  if (PreferencesResolver.isSMSResponseEnabled(context) && incomingNumber != null) {
			log.info("Sending an SMS to " + incomingNumber);
			String smsText = PreferencesResolver.getSMSResponseText(context);

			SmsManager smsManager = SmsManager.getDefault();
			smsManager.sendTextMessage(incomingNumber, null, smsText, null, null);
		  }
		}
		break;
	  }
	}
  };
}
